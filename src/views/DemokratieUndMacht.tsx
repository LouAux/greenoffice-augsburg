import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap-icons/font/bootstrap-icons.css";

export function DemokratieUndMacht() {
  return (
    <>
      <h1 id="Demokratieverständnis & Macht">Demokratieverständnis & Macht</h1>
      <br></br>
      <p>
        Die Klimakatastrophe ist ein globales und komplexes Problem, für das
        hauptsächlich globale Lösungen gefordert werden. Maßnahmen beschränken
        sich aktuell auf technologische Ansätze, um ein 'Weiter so' zu
        ermöglichen, das auf Wirtschaftswachstum und der Sicherung des
        "Wohlstands" basiert. Durch den Fokus der "internationalen Klimapolitik"
        auf globale Ansätze geraten lokale Lösungsstrategien und Fragen der
        sozialen Gerechtigkeit aus dem Blick. Die Menschen haben das Gefühl, ihr
        Handeln mache keinen Unterschied und jede Veränderung sei eine Gefahr
        für den "wohlverdienten Wohlstand". Doch von welchem Wohlstand sprechen
        wir, wenn Ungleichheit stetig wächst und bereits heute Menschen an den
        Folgen der Klimakatastrophe sterben? Scheinbar ist hiermit nur der
        "Wohlstand" des Globalen Nordens gemeint.
        <br></br>
        <br></br>
        <i>Macht</i>
        <br></br>
        Um den Fokus auf Wirtschaftswachstum und den Wohlstand von Wenigen
        aufzubrechen, ist ein grundlegender Wandel nötig. Wenn Menschen etwas in
        der Gesellschaft verändern wollen, müssen sie sich ihrer Macht
        bewusstwerden. Diese nutzen sie, wenn sie bestehende Schwierigkeiten
        erkennen und Alternativen aufzeigen. Sie entfalten Gestaltungspotenzial,
        wenn sie sich treffen und gemeinsam aktiv werden - Stichwort
        Empowerment. Diese Vor-Ort-Initiativen können sogar globale Auswirkungen
        haben. Dafür ist es wichtig, von anderen zu lernen, Ideen auszutauschen
        und die verschiedenen lokalen Ansätze zu vernetzen.
        <br></br>
        <br></br>
        Unserer Ansicht nach ist dabei eine dekoloniale und postkoloniale
        (Fußnote) Perspektive unerlässlich. Das bedeutet für uns, über den
        eigenen Erfahrungshorizont hinauszublicken und wahrzunehmen, was bereits
        praktiziert wird. Denn überall auf der Welt gibt es Beispiele, wie ein
        Leben in Gemeinschaft und mit Respekt vor der Natur gelingen kann
        (Fussnote: Rojava & Zapatistas, Buen Vivir). Dafür ist Pluralität
        wichtig: Damit alle Menschen gehört werden, müssen bestehende
        Machtstrukturen und historisch geschaffene Ungerechtigkeit - zwischen
        Globalem Norden und Globalem Süden sowie innerhalb einzelner
        Gesellschaften - kritisch hinterfragt werden. Grundlage dafür ist die
        Reflexion unserer eigenen Privilegien. Nur so können wir Strukturen
        verändern und Ressourcen und Zugang zu Macht gerechter verteilten.
        <br></br>
        <br></br>
        <i>Demokratie</i>
        <br></br>
        Momentan ist Macht ungleich verteilt. [Fußnote: Dies merken wir an der
        andauernden Unterdrückung und Benachteiligung von Menschen,
        beispielsweise in Form von Rassismus, Ableismus, Sexismus, Klassismus
        -und anderer Lebewesen in Form von Speziesismus. Diese
        Ungleichbehandlung ist weder "natürlich" noch "natürlich gewachsen".
        Unterdrückung wird in unserem System reproduziert, um Hierarchien
        aufrecht zu erhalten. Außerdem verhindern und verschleiern
        Unterdrückungsmechanismen unsere Handlungs- und Gestaltungsspielräume.
        Wir können sie dadurch schlecht bzw. gar nicht wahrnehmen. Dies
        erschwert die Auflösung der Unterdrückung und hilft dem System, sich
        selbst zu erhalten. Wir wollen diese Handlungsspielräume wieder sichtbar
        machen und vorantreiben.] Diese geht hauptsächlich vom neoliberalen
        Paradigma aus, das politische und individuelle Handlungen leitet.
        Festgefahrene Strukturen und zementierte Machtverhältnisse engen den
        Gestaltungsspielraum der Menschen ein. Dieser ist aber ein wesentlicher
        Baustein der Demokratie. Ein politisches System, in dem die Menschen
        keine Partizipationsmöglichkeiten haben, ist keine Demokratie.
        <br></br>
        <br></br>
        Demokratie bedeutet für uns, dass alle Entscheidungskraft von der
        Gemeinschaft ausgeht. Das impliziert einen kollektiven Austausch über
        Prinzipien, Werte und gemeinsame Ziele. Um Partizipationsmöglichkeiten
        zu schaffen, braucht es einen gleichberechtigten Zugang zu
        demokratischen Mitteln, die Verfügbarkeit von Informationen und
        Transparenz von Entscheidungsprozessen. Eine solche gelebte
        Basisdemokratie ist ein Ideal, das aktiv ausgearbeitet und entwickelt
        werden muss. Lasst uns gemeinsam diesen Prozess gestalten!
        <br></br>
        <br></br>
      </p>
    </>
  );
}
