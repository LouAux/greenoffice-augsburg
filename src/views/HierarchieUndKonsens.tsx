import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap-icons/font/bootstrap-icons.css";

export function HierarchieUndKonsens() {
  return (
    <>
      <h1 id="Hierarchie & Konsens">Hierarchie & Konsens</h1>
      <br></br>
      <div>
        <p>
          Der Wert eines Menschen wird oft abhängig von der hierarchischen
          Position wahrgenommen. Dabei stehen Hierarchien in einer engen
          Wechselwirkung mit Privilegien. Das eine bedingt das andere. Die
          hierarchischen Strukturen sind historisch gewachsen und systemisch
          verankert. Individuen verinnerlichen und reproduzieren diese
          unbewusst.
          <br></br>
          <br></br>
          Wir unterscheiden allgemein zwischen Wissens- und Machthierarchien:
          <br></br>
        </p>
        <ul>
          <li>
            Wissenshierarchien entstehen dadurch, dass Menschen unterschiedliche
            Fähigkeiten, Bildungschancen und ungleiche Informationszugänge
            haben.
          </li>
          <li>
            Machthierarchien sind auf Grund historischer Kontinuitäten
            systemisch immanent und werden verstärkt, wenn Wissenshierarchien
            genutzt werden, um Macht über andere auszuüben. Privilegierte
            Menschen haben mehr Mittel zur Verfügung, um ihre eigenen Interessen
            durchzusetzen.
          </li>
        </ul>
        <p>
          <br></br>
          Menschen werden durch gewohnte Hierarchien dazu verleitet, bestehende
          Verhältnisse nicht zu hinterfragen und Verantwortung abzugeben.
          Veränderung setzt aber voraus, dass Menschen diese Machtverhältnisse
          reflektieren und überwinden. Unser Ziel ist es, Wissenshierarchien
          durch Transparenz, Skillshares und Workshops abzubauen, damit daraus
          keine Machthierarchien entstehen können.
          <br></br>
          <br></br>
          Hierarchie ist ein Über- und Unterordnen von Menschen. Sie verhindert
          ein Begegnen auf Augenhöhe und die Selbstwirksamkeit der Individuen.
          Wir lehnen diese Strukturen ab, weil dadurch verschiedenen Menschen
          ein unterschiedlicher Wert beigemessen wird. Ein respektvoller Umgang
          miteinander kann niemals auf Hierarchien basieren. Damit alle Menschen
          einen freien und gleichen Zugang zu Entscheidungsprozessen haben,
          streben wir ein möglichst hierarchiefreies Zusammenleben an. Aus
          unserer Perspektive ermöglichen Konsensentscheidungen die größte
          Freiheit für alle. Gemeinsam als Gruppe Entscheidungen im Konsens zu
          treffen bedeutet, dass diese von allen mitgetragen und Bedenken jede*r
          Einzelnen berücksichtigt werden.
          <br></br>
          <br></br>
          Entsteht ein Raum, in dem sich alle auf Augenhöhe begegnen, spüren die
          Menschen eine Wertschätzung für ihre Interessen und Bedenken und sind
          bereit in eine gewaltfreie und fruchtbare Kommunikation zu treten.
          Dieser Prozess fördert die Zusammenarbeit und das Verständnis
          füreinander.
          <br></br>
          <br></br>
          Manchmal kann es schwierig sein, alle unterschiedlichen Interessen und
          Prioritäten unter einen Hut zu bringen. Wenn aber ein Raum entsteht,
          in dem sich alle auf Augenhöhe begegnen, spüren die Menschen eine
          Wertschätzung für ihre Interessen und Bedenken und sind bereit in eine
          gewaltfreie und fruchtbare Kommunikation zu treten. Dieser Prozess
          fördert die Zusammenarbeit und das Verständnis füreinander. Da alle
          Perspektiven berücksichtigt werden, sind kollektive Entscheidungen
          wirksamer und nachhaltiger.
        </p>
      </div>
      <br></br>
      <br></br>
    </>
  );
}
