import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap-icons/font/bootstrap-icons.css";

export function NarrativUndZukunftsvision() {
  return (
    <>
      <h1 className="mt-5">Narrativ & Zukunftsvision</h1>
      <br></br>
      <div>
        <p>
          <i>Wer wir sind, was wir wollen & warum</i>
          <br></br>
          <br></br>
          Unsere Gruppe setzt sich mit der Klimakatastrophe auseinander und will
          Teil einer Lösung sein. Dabei ist diese Katastrophe unserer Ansicht
          nach nur ein Symptom tiefgreifender Probleme. Hervorgerufen wird sie
          durch eine alles durchdringende Wirtschafts- und Wachstumslogik, die
          sich durch Individualisierung und die Trennung von Mensch und Natur
          ausdrückt. Menschen werden nicht mehr innerhalb von Gemeinschaften
          sozialisiert, sondern von einem individualistischen System geprägt,
          was zu Ignoranz, fehlender Weitsicht und dem Wegschieben von
          Verantwortung führt. Die Auswirkungen treffen aufgrund bestehender
          Hierarchien und daraus resultierender globaler Abhängigkeiten
          gesellschaftlich marginalisierte Menschen besonders hart.
          <br></br>
          <br></br>
          Wir haben gesehen, dass die Politik nicht handeln wird. Jegliche
          Veränderung wurde in der Geschichte durch Druck von unten erkämpft.
          Daher werden wir nicht länger Schilder hochhalten. Wir wollen nicht
          länger den Wind machen, in den die Politik ihre Segel spannt. Wir
          wollen einen Wind erzeugen, der so stark ist, dass es notwendig wird,
          gemeinsam neue, stabilere Segel zu knüpfen - bevor die
          Klimakatastrophe diese von allein zerreißt und uns keine Zeit bleibt,
          neue zu weben.
          <br></br>
          <br></br>
          <br></br>
          Angesichts dieses komplexen Geflechts aus gesellschaftlicher
          Ungleichheit, Machtstrukturen und Diskriminierungssystemen ist keine
          einfache vorgefertigte Lösung möglich. Vielmehr braucht es eine
          Erschütterung, um kapitalistische, patriarchale und koloniale Logiken
          zu durchbrechen und Ungerechtigkeit abzubauen. Grundlage für
          Veränderung ist neben einem Wandel des ökonomischen Systems auch ein
          Wertewandel. Erst durch das Stärken eines allgemeinen Bewusstseins für
          die Endlichkeit unserer planetaren Ressourcen und den Stellenwert der
          Gemeinschaft wird sich auch eine Veränderung unserer Art des
          Wirtschaftens ergeben. Ein solcher radikaler Umschwung kann nur
          gelingen, wenn wir Strukturen schaffen, um andere Formen des
          Zusammenlebens zu ermöglichen.
          <br></br>
          <br></br>
          Viele Menschen fühlen sich machtlos. Sie haben das Gefühl, nichts
          verändern zu können, sei es aufgrund fehlender Ressourcen,
          Abhängigkeiten und Zwängen in festgefahrenen Strukturen oder dem
          Gefühl, allein zu sein. Wir sind aber überzeugt, dass jede*r fähig
          ist, zu handeln und einen Unterschied zu machen. Keine*r ist machtlos
          - gerade Menschen, die auf unterschiedlichen Ebenen diskriminiert und
          unterdrückt werden, schließen sich zusammen und bewegen im Kollektiv
          viel. Dennoch ist es unerlässlich, Ressourcen anders zu verteilen und
          somit die Handlungsmacht aller zu stärken.
          <br></br>
          <br></br>
          Wir als Gruppe wollen Menschen ihre Macht aufzeigen und sie ermutigen,
          aktiv zu werden. So wollen wir der Ohnmacht entgegenwirken und
          Alternativen aufzeigen, die das fehlerhafte System, in dem wir leben,
          ersetzen können. Dazu gehört, durch gezielte Systemkritik einen Bruch
          mit binären Denkweisen und Strukturen herbeizuführen, um neue Wege des
          Handelns zu eröffnen. Kurz gesagt wollen wir informieren, aktivieren
          und revolutionieren:
          <br></br>
        </p>
        <ul>
          <li>
            Informieren: Bildungsangebote, Bewusstsein und Orte der Reflexion
            schaffen
          </li>
          <li>
            Aktivieren: Lokale Strukturen aufbauen, Kollektive bilden, Menschen
            untereinander vernetzen, Synergieeffekte nutzen, Wissen sammeln und
            weitergeben
          </li>
          <li>
            Revolutionieren: Alternativen finden und aufzeigen, neue Formen des
            Zusammenlebens miteinander entwickeln, der Gemeinschaft einen neuen
            Stellenwert geben, neue Strukturen etablieren
          </li>
        </ul>
        <p>
          <br></br>
          Ein gewaltfreier Umgang der Menschen untereinander und mit der Natur
          ist Teil und Ziel dieses Prozesses. <br></br>
          Es ist unerlässlich, dass wir dabei stets unsere Position, Grenzen und
          Privilegien reflektieren.
        </p>
      </div>
      <br></br>
      <br></br>
    </>
  );
}
