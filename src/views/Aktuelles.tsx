import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "bootstrap-icons/font/bootstrap-icons.css";

export function Aktuelles() {
  return (
    <>
      <div className="mt-3">
        <i>01.06.2023</i>
        <br />
        <p>
          Kollektiv R ist am Start! <br></br>
          <br></br>
          Schluss mit Students For Future Augsburg.  <br></br>
          <br></br>
        </p>
        <br />
      </div>
      <hr />
      <br />
    </>
  );
}
